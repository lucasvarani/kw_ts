class AddOrdemToAdminEnterprises < ActiveRecord::Migration
  def change
    add_column :admin_enterprises, :ordem, :integer, :default => 10
  end
end
