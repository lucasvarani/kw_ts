class AddCityIdToEnterprises < ActiveRecord::Migration
  def change
    add_reference :admin_enterprises, :city, index: true
  end
end
