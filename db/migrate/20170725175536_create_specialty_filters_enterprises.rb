class CreateAdminEnterprisesSpecialtyFilters < ActiveRecord::Migration
  def change
    create_table :admin_enterprises_specialty_filters do |t|
      t.integer :specialty_filter_id
      t.integer :enterprise_id
    end
  end
end
