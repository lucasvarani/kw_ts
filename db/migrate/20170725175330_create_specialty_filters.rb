class CreateSpecialtyFilters < ActiveRecord::Migration
  def change
    create_table :specialty_filters do |t|
      t.string :name
    end
  end
end
