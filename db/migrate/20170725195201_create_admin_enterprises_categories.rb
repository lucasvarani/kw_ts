class CreateAdminEnterprisesCategories < ActiveRecord::Migration
  def change
    create_table :admin_enterprises_categories do |t|
      t.integer :enterprise_category_id
      t.integer :enterprise_id
    end
  end
end
