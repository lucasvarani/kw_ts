class AddElaborationDeliveryServiceToEnterpriseRates < ActiveRecord::Migration
  def change
    add_column :enterprise_rates, :elaboration, :integer
    add_column :enterprise_rates, :delivery, :integer
    add_column :enterprise_rates, :service, :integer
  end
end
