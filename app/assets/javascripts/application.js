// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jQuery/jquery-2.2.3.min.js
//= require jquery_ujs
//= require bootstrap.js
// require turbolinks
//= require lib/moment.min.js
//= require fullcalendar.min.js
//= require locale-all.js
//= require jquery.fancybox.js
//= require jquery.fancybox-thumbs.js
//= require ckeditor/init
// require tinymce-jquery
// require_tree .
//= require states_and_cities
//= require select2/select2.full.min.js

$(document).ready(function() {

  $(".enterprise_state").select2();
  $(".filter-state").select2();
  $(".filter-city").select2();
  $(".specialty_select").select2();

  if ($("#enterpriseRow")) {
    size = $("#enterprises .enterpriseRow").size();
      x=8;
      $('#showLess').hide();
      $('#enterprises .enterpriseRow:lt('+x+')').show();
      $('#loadMore').click(function () {
        x= (x+10 <= size) ? x+10 : size;
        $('#enterprises .enterpriseRow:lt('+x+')').show();
         $('#showLess').show();
        if(x == size){
            $('#loadMore').hide();
        }
      });
      $('#showLess').click(function () {
        x=(x-10<0) ? 8 : x-10;
        $('#enterprises .enterpriseRow').not(':lt('+x+')').hide();
        $('#loadMore').show();
         $('#showLess').show();
        if(x == 8){
            $('#showLess').hide();
        }
      });
  }

});

// $(document).on('page:load', ready);