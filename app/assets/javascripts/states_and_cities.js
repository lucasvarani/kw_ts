$(document).ready(function() {
  $("#state_id").change(function() {
    getCitiesByState("id="+$("#state_id").val());
  });


  function getCitiesByState(id) {
    $.getJSON("/cities_by_state", id, function(j) {
      var options = '<option value="">Selecione sua cidade</option>';

      $.each(j.cty, function(i, item) {
        options += '<option value="' + item.id + '">' + item.name + '</option>';
      });
      $("select[id$=_city_id]").html(options);
    });
  }
});
