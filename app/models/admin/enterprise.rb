class Admin::Enterprise < ActiveRecord::Base
	acts_as_taggable_on :tags
	validates_presence_of :name, :description,:address, :number, :phone, :email, :site

	has_attached_file :img_enterprise, styles: {
	                                  medium: "226x160#",
	                                  small: "100x100#"
	                                  },
	                                  default_url: "/images/user_missing_:style.png"

  validates_attachment :img_enterprise, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }

	has_attached_file :banner_enterprise, styles: {
	                                  medium: "226x160#",
	                                  small: "100x100#"
	                                  },
	                                  default_url: "/images/user_missing_:style.png"

	validates_attachment :banner_enterprise, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }

	has_many :portfolios
	has_and_belongs_to_many :wikis
	has_and_belongs_to_many :specialities
	has_and_belongs_to_many :specialty_filters
	has_and_belongs_to_many :enterprise_categories
	has_many :enterprise_rates
	belongs_to :city


	def self.search(product, category, speciality, city, state, q, exceptions = nil)
	  enterprises = Admin::Enterprise.where(published: true)
	  enterprises = enterprises.where("name LIKE ?","#{q}%" ) if q.present?
	  return enterprises if q.present?
	  enterprises = enterprises.joins(:specialty_filters).where(:specialty_filters => {name: speciality}) if speciality.present?
	  enterprises = enterprises.joins(:enterprise_categories).where(:enterprise_categories => {name: category}) if category.present?
	  enterprises = enterprises.joins(:specialities).where(:specialities => {name: product}) if product.present?
	  enterprises = enterprises.includes(:city).where('cities.name = ?', city).references(:city) if city.present?
	  enterprises = enterprises.includes(city: :state).where('states.name = ?', state).references(:city).references(:state) if state.present?
	  return enterprises
	end

	def self.search_test(q, exceptions = nil)
    search_empresas = Admin::Enterprise.where(published: true)
    search_empresas_1 = search_empresas.joins(:specialities).where("specialities.name LIKE ?", "%#{q}%").uniq
    return search_empresas_1 if search_empresas_1.present?
    search_empresas_2 = search_empresas.where("admin_enterprises.name LIKE ?", "%#{q}%")
    return search_empresas_2 if search_empresas_2.present?
    search_empresas_3 = search_empresas.where("admin_enterprises.description LIKE ?", "%#{q}%")
    return search_empresas_3 if search_empresas_3.present?
  end

end
