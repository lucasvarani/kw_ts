class Notifier < ApplicationMailer
  def send_sugestion(sugestion)
    @sugestion = sugestion

    user = User.find(@sugestion.user_id)
    mail(:to => "vanderlei.marques@trainingsociety.com.br", :bcc => "log@korewa.com.br", :subject => "Nova sugestão enviada por #{user.name} #{user.last_name} ")

  end

  def password_reset_instructions(user)
    @edit_password_reset_url = edit_password_reset_path(user.perishable_token)
    mail(:to => "#{user.name} <#{user.email}>", :subject => "Instruções para resetar a senha", :from => "Training Society <no-reply@sato7.com.br>")
  end

  def new_enterprise(message)
    @message = message
    mail(:to => "vanderlei.marques@trainingsociety.com.br", :subject => "Uma nova empresa quer se cadastrar no Training Society!", :from => "Training Society <no-reply@sato7.com.br>")
  end

  def contact(message)
    @message = message
    mail(:to => "vanderlei.marques@trainingsociety.com.br", :subject => "Uma nova mensagem enviado pelo site -  Training Society!", :from => "Training Society <no-reply@sato7.com.br>")
  end

  def new_user(user)
    @user = user
    mail(:to => "vanderlei.marques@trainingsociety.com.br", :subject => "Um novo usuário se cadastrou no Training Society!", :from => "Training Society <no-reply@sato7.com.br>")
  end

end
