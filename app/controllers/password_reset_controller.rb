class PasswordResetController < ApplicationController
  #before_filter :require_no_user
  before_filter :load_user_using_perishable_token, :only => [:edit, :update]

  layout 'home'

  def new
    @user = User.new
    @user_session = UserSession.new
    render
  end

  def create
    @user = User.new
    @user_session = UserSession.new
    @user = User.find_by_email(params[:email])

    if @user
      @user.deliver_password_reset_instructions!
      flash[:notice] = "As instruções para redefinir sua senha foram enviadas para você. \
        Por favor verifique seu email."
      redirect_to root_url
    else
      flash[:notice] = "Nenhum usuário foi encontrato com esse e-mail!"
      render :action => :new
    end
  end

  def edit
    @user = User.new
    @user_session = UserSession.new
    render
  end

  def update
    @user.password = params[:user][:password]
    @user.password_confirmation = params[:user][:password_confirmation]

    if @user.save
      flash[:notice] = "Senha atualizada com sucesso!"
      redirect_to profiles_path
    else
      render :action => :edit
    end
  end

  private

  def load_user_using_perishable_token
    @user = User.find_using_perishable_token(params[:id])

    unless @user
      flash[:notice] = "Desculpe, mas não conseguimos localizar sua conta. Se você está tendo problemas, tente copiar e colar o URL do seu e-mail para o navegador ou reiniciar o processo de redefinição de senha."
      redirect_to root_url
    end
  end
end
