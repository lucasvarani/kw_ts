class EmpresasController < ApplicationController
  access_control do
    allow logged_in
  end

  def index
    # @empresas = Admin::Enterprise.where(["name LIKE ? AND published = 1", "a%"]).order(:name => "ASC")
    if params[:q] or params[:product] or params[:category] or params[:speciality] or params[:city] or params[:state]
      @empresas = Admin::Enterprise.search(params[:product], params[:category], params[:speciality] ,params[:city], params[:state], params[:q]).order(:ordem => "asc", :name => "asc")
    else
      @empresas = Admin::Enterprise.search(params[:product], params[:category], params[:speciality] ,params[:city], params[:state], params[:q]).order(:ordem => "asc", :name => "asc").limit(10)
    end
    if params[:state] && params[:state] != ""
      state = params[:state]
      @state = State.where(["name = '#{state}'"]).last
    end
    if !@empresas.present?
      params[:city] = ""
      @empresas = Admin::Enterprise.search(params[:product], params[:category], params[:speciality] ,params[:city], params[:state], params[:q]).order(:ordem => "asc", :name => "asc")
    end
  end

  def show
    @empresa = Admin::Enterprise.find(params[:id])
    @portfolios = @empresa.portfolios.where(:published => true, :active => true)
    if @empresa.enterprise_rates.present?
      @media = @empresa.enterprise_rates.all.average(:rate).round
      @media_elaboration = @empresa.enterprise_rates.all.average(:elaboration).round
      @media_service = @empresa.enterprise_rates.all.average(:service).round
      @media_delivery = @empresa.enterprise_rates.all.average(:delivery).round
    end
  end

  # def tags
  #   @speciality = Speciality.find(params[:id])
  #   @empresas = @speciality.enterprises.all
  # end

  # def filters
  #   @empresas = Admin::Enterprise.where(["name LIKE ? OR state LIKE ? AND published = 1", "#{params[:id]}%", "#{params[:id]}%"]).order(:name => "ASC")
  # end
end
