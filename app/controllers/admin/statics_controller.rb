class Admin::StaticsController < ApplicationController
  access_control do
      allow :administrator, :all
  end

  layout "admin"

  def index
    @enterprises = Admin::Enterprise.all
    @users = User.all
    @articles = Article.all
    @suggestions = Suggestion.all
  end
end
