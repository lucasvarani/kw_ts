class Admin::SpecialitiesController < ApplicationController
  layout "admin"
  before_action :set_speciality, except: [:index, :new, :create]
  # GET /admin/categories
  # GET /admin/categories.json
  def index
    @specialities = Speciality.all
  end

  # GET /admin/categories/1
  # GET /admin/categories/1.json
  def show
  end

  # GET /admin/categories/new
  def new
    @speciality = Speciality.new
  end

  # GET /admin/categories/1/edit
  def edit
  end

  # POST /admin/categories
  # POST /admin/categories.json
  def create
    @speciality = Speciality.new(admin_specialities_params)

    respond_to do |format|
      if @speciality.save
        format.html { redirect_to admin_speciality_path(@speciality), notice: 'Speciality was successfully created.' }
        format.json { render :show, status: :created, location: @speciality }
      else
        format.html { render :new }
        format.json { render json: @speciality.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/categories/1
  # PATCH/PUT /admin/categories/1.json
  def update
    respond_to do |format|
      if @speciality.update(admin_specialities_params)
        format.html { redirect_to admin_speciality_path(@speciality), notice: 'Speciality was successfully updated.' }
        format.json { render :show, status: :ok, location: @speciality }
      else
        format.html { render :edit }
        format.json { render json: @speciality.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/categories/1
  # DELETE /admin/categories/1.json
  def destroy
    @speciality.destroy
    respond_to do |format|
      format.html { redirect_to admin_specialities_url, notice: 'Speciality was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_speciality
      @speciality = Speciality.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_specialities_params
      params.require(:speciality).permit(:name)
    end
end
