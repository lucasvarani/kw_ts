class ContactsController < ApplicationController

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        Notifier.contact(@contact).deliver
        format.html { redirect_to root_path, notice: "Mensagem enviada com sucesso! entraremos em contato o mais breve possível!" }
      else
        flash[:alert] = "Ocorreu um erro ao enviar o e-mail."
        format.html { redirect_to root_path }
      end
    end
  end

private

  def contact_params
    params.require(:contact).permit(:name, :email, :message)
  end

end