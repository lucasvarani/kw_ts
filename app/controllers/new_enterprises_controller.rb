class NewEnterprisesController < ApplicationController

  def new
    @new_enterprise = NewEnterprise.new
  end

  def create
    @new_enterprise = NewEnterprise.new(new_enterprise_params)

    if @new_enterprise.valid?
      Notifier.new_enterprise(@new_enterprise).deliver
      redirect_to root_path, notice: "Mensagem enviada com sucesso! entraremos em contato o mais breve possível!"
    else
      flash[:alert] = "Ocorreu um erro ao enviar o e-mail."
      redirect_to root_path
    end
  end

private

  def new_enterprise_params
    params.require(:new_enterprise).permit(:name, :email, :phone, :message)
  end

end