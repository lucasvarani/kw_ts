class EventosController < ApplicationController
  access_control do
    allow logged_in
  end
  
  def index
    @eventos = Admin::Organization.all

    # @datas = ""

    # @e = @eventos.last
    
    # @eventos.each do |e|
    #   start = (e.organization_start.strftime("%Y-%m-%dT%H:%M:%S")).to_s
    #   finish = (e.organization_finish + 1).to_s
    #   @datas += "{"
    #   @datas += "title: '#{e.title}',"
    #   @datas += "start: '#{start}',"
    #   @datas += "end: '#{finish}'"
    #   @datas += "},"
    # end
    
    # puts "==============DATAS #{@datas.inspect}"
    
    # @galerias = Admin::Organization.all.order("id DESC").limit(4)
  end

  def show
    @evento = Admin::Organization.find(params[:id])  
  end

end
