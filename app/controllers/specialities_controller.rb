class SpecialitiesController < ApplicationController
  layout "admin"
  # GET /admin/categories
  # GET /admin/categories.json
  def index
    @specialities = Speciality.all
  end

  # GET /admin/categories/1
  # GET /admin/categories/1.json
  def show
  end

  # GET /admin/categories/new
  def new
    @specialities = Speciality.new
  end

  # GET /admin/categories/1/edit
  def edit
  end

  # POST /admin/categories
  # POST /admin/categories.json
  def create
    @specialities = Speciality.new(admin_speciality_params)

    respond_to do |format|
      if @specialities.save
        format.html { redirect_to @specialities, notice: 'Speciality was successfully created.' }
        format.json { render :show, status: :created, location: @specialities }
      else
        format.html { render :new }
        format.json { render json: @specialities.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/categories/1
  # PATCH/PUT /admin/categories/1.json
  def update
    respond_to do |format|
      if @specialities.update(admin_speciality_params)
        format.html { redirect_to @specialities, notice: 'Speciality was successfully updated.' }
        format.json { render :show, status: :ok, location: @specialities }
      else
        format.html { render :edit }
        format.json { render json: @specialities.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/categories/1
  # DELETE /admin/categories/1.json
  def destroy
    @specialities.destroy
    respond_to do |format|
      format.html { redirect_to admin_specialities_url, notice: 'Speciality was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_speciality
      @specialities = Speciality.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_specialities_params
      params.require(:specialities).permit!
    end
end
